﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent (typeof(WeaponManager))]
public class PlayeShooty : NetworkBehaviour {

    private const string PLAYER_TAG = "Player";
    private const string HEAD_TAG = "Head";



    [SerializeField]
    private Camera cam;

    [SerializeField]
    private Camera WeaponCam;

    [SerializeField]
    private LayerMask mask;

    [SerializeField]
    private int criticalHitMultiplyer = 3;

    private PlayerWeapons currentWeapon;
    private WeaponManager weaponManager;
    private float playerFOV;
    private float timeSinceLastShot;
    private AudioSource weaponAudioSource;

    private void Start()
    {
        if(cam == null)
        {
            Debug.LogError("PlayerShooty: No camera referenced");
            this.enabled = false;
        }

        weaponManager = GetComponent<WeaponManager>();

        playerFOV = cam.fieldOfView;
    }

    private void Update()
    {
        currentWeapon = weaponManager.GetCurrentWeapon();

        timeSinceLastShot += Time.deltaTime;

        if (PauseMenu.isPaused) return;

        if (currentWeapon.fireRate <= 0f)
        {
            if (Input.GetButtonDown("Fire1") && timeSinceLastShot > currentWeapon.delay)
            {
                Shoot();
                timeSinceLastShot = 0;
            }
        }
        else
        {
            if (Input.GetButtonDown("Fire1"))
            {
                InvokeRepeating("Shoot", 0f, 1f/currentWeapon.fireRate);
            }
            else if (Input.GetButtonUp("Fire1"))
            {
                CancelInvoke("Shoot");
            }
        }

        if (Input.GetButton("Fire2"))
        {
            ZoomIn();
        }
        else
        {
            ZoomOut();
        }

    }

    private void ZoomIn()
    {
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, 20, 10 * Time.deltaTime);
        WeaponCam.fieldOfView = Mathf.Lerp(cam.fieldOfView, 20, 10 * Time.deltaTime);
    }
    private void ZoomOut()
    {
        cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, playerFOV, 10 * Time.deltaTime);
        WeaponCam.fieldOfView = Mathf.Lerp(cam.fieldOfView, playerFOV, 10 * Time.deltaTime);
    }

    //Called on the server when a player shoots
    [Command]
    void CmdOnShoot()
    {
        RpcDoShootEffect();
    }

    //Called on the server when something is hit
    //Takes in hit point and the normal of the surface
    [Command]
    void CmdOnHit(Vector3 pos, Vector3 normal)
    {
        RpcDoImpactEffect(pos, normal);
    }

    //Called on all clinets when we need to do a shoot effect
    [ClientRpc]
    void RpcDoShootEffect()
    {
        weaponManager.GetCurrentGraphics().muzzleFlash.Play();
        weaponManager.GetCurrentGraphics().animator.Play("Fire");
        weaponManager.GetCurrentAudio().Play();
    }

    [ClientRpc]
    void RpcDoImpactEffect(Vector3 pos, Vector3 normal)
    {
        GameObject hitEffect = Instantiate(weaponManager.GetCurrentGraphics().hitEffectPrefab, pos, Quaternion.LookRotation(normal));
        Destroy(hitEffect, 3f);
    }

    [Client]
    private void Shoot()
    {
        if (!isLocalPlayer) return;
        CmdOnShoot();

        RaycastHit hit;
        if (Physics.Raycast(cam.transform.position, cam.transform.forward, out hit, currentWeapon.range, mask))
        {
            if(hit.collider.tag == PLAYER_TAG)
            {
                CmdPlayerShot(hit.collider.name, currentWeapon.damage, transform.name);
            }
            else if(hit.collider.tag == HEAD_TAG)
            {
                CmdPlayerShot(hit.transform.name, currentWeapon.damage * criticalHitMultiplyer, transform.name);
            }

            
            CmdOnHit(hit.point, hit.normal);
        }
    }

    [Command]
    void CmdPlayerShot(string playerId, int damage, string source)
    {
        Debug.Log(playerId + " Has been shot by " + source);

        Player player = GameManager.GetPlayer(playerId);

        player.RpcTakeDamage(damage, source);
    }
}
