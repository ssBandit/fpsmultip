﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerNameSetter : MonoBehaviour {

    public string playerUsername = "???";

    public void SetName(string username)
    {
        playerUsername = username;
    }
}
