﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(Player))]
public class PlayerSetup : NetworkBehaviour {

    [SerializeField]
    Behaviour[] componentsToDisable;

    [SerializeField]
    string remoteLayerName = "Remote Player";

    [SerializeField]
    string dontDrawLayeName = "DontDraw";

    [SerializeField]
    GameObject playerGraphics;

    [SerializeField]
    GameObject playerUIPrefab;
    [HideInInspector]
    public GameObject playerUIInstance;


    private void Start()
    {
        if (!isLocalPlayer)
        {
            DisableComponents();
            AssignRemoteLayer();
        }
        else
        {
            
            //Disable player graphics
            SetLayerRecursivly(playerGraphics, LayerMask.NameToLayer(dontDrawLayeName));

            //Create playerUI
            playerUIInstance = Instantiate(playerUIPrefab);
            playerUIInstance.name = playerUIPrefab.name;

            GetComponent<Player>().SetupPlayer();

            if (NetworkManager.singleton.GetComponent<PlayerNameSetter>().playerUsername != null)
            {

                GetComponent<Player>().username = NetworkManager.singleton.GetComponent<PlayerNameSetter>().playerUsername;
            }
            else
            {
                GetComponent<Player>().username = transform.name;
            }
            


            PlayerUI ui = playerUIInstance.GetComponent<PlayerUI>();

            ui.SetPlayer(GetComponent<Player>());

            
        }
    }
    

    private void SetLayerRecursivly(GameObject obj, int newLayer)
    {
        obj.layer = newLayer;

        foreach (Transform child in obj.transform)
        {
            SetLayerRecursivly(child.gameObject, newLayer);
        }
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        string netID = GetComponent<NetworkIdentity>().netId.ToString();
        Player player = GetComponent<Player>();

        GameManager.RegisterPlayer(netID, player);
    }


    void AssignRemoteLayer()
    {
        gameObject.layer = LayerMask.NameToLayer(remoteLayerName);
    }

    void DisableComponents()
    {

        foreach (Behaviour component in componentsToDisable)
        {
            component.enabled = false;
        }

    }

    private void OnDisable()
    {
        Destroy(playerUIInstance);

        if (isLocalPlayer)
        {
            GameManager.instance.SetSceneCameraActive(true);
        }

        GameManager.UnRegisterPlayer(transform.name);
    }

}
