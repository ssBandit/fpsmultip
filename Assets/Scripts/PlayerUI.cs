﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUI : MonoBehaviour {

    [SerializeField] RectTransform healthBarfill;

    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject scoreboard;

    private Player player;

    public void SetPlayer(Player _player)
    {
        player = _player;
    }

    private void Start()
    {
        PauseMenu.isPaused = false;
    }

    void Update ()
    {
        if (player == null) return;
        SetHealthAmount(player.getHealthPercent());

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePauseMenu();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            scoreboard.SetActive(true);
        }
        else if (Input.GetKeyUp(KeyCode.Tab))
        {
            scoreboard.SetActive(false);
        }
		
	}

    public void TogglePauseMenu()
    {
        pauseMenu.SetActive(!pauseMenu.activeSelf);
        PauseMenu.isPaused = pauseMenu.activeSelf;
    }

    void SetHealthAmount(float amount)
    {
        healthBarfill.localScale = new Vector3(1f, amount, 1f);
    }
}
