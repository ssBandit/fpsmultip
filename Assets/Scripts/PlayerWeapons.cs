﻿using UnityEngine;

[System.Serializable]
public class PlayerWeapons
{
    public string name = "Rifle";

    public int damage = 10;
    public float range = 100f;

    public float fireRate = 0f;

    public float delay = 2f;

    public AudioClip shotSound;

    public GameObject graphics;

}
