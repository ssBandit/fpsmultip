﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scoreboard : MonoBehaviour {

    [SerializeField] GameObject playerScoreboardItemPrefab;
    [SerializeField] Transform playerScoreboardList;

    private void OnEnable()
    {
        // Get an array of players
        Player[] players = GameManager.GetAllPlayers();

        foreach (Player player in players)
        {
            //Debug.Log(player.name + " | " + player.kills + " | " + player.deaths);

            GameObject itemGO = Instantiate(playerScoreboardItemPrefab, playerScoreboardList);
            PlayerScoreboardItem item = itemGO.GetComponent<PlayerScoreboardItem>();

            if (item != null)
            {
                item.Setup(player.username, player.kills, player.deaths);
            }

        }

    }

    private void OnDisable()
    {
        foreach (Transform child in playerScoreboardList)
        {
            Destroy(child.gameObject);
        }
    }
}
