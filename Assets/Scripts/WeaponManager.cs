﻿using UnityEngine;
using UnityEngine.Networking;

public class WeaponManager : NetworkBehaviour
{
    [SerializeField]
    string weaponLayerName = "Weapon";

    [SerializeField]
    private Transform weaponHolder;

    [SerializeField]
    private PlayerWeapons primaryWeapon;
    private PlayerWeapons currentWeapon;

    private WeaponGraphics currentGraphics;
    private AudioSource currentAudio;


    private void Start()
    {
        EquipWeapon(primaryWeapon);
    }

    public PlayerWeapons GetCurrentWeapon()
    {
        return currentWeapon;
    }

    public WeaponGraphics GetCurrentGraphics()
    {
        return currentGraphics;
    }

    public AudioSource GetCurrentAudio()
    {
        return currentAudio;
    }

    void EquipWeapon(PlayerWeapons weapon)
    {
        currentWeapon = weapon;

        GameObject weaponIns = Instantiate(weapon.graphics, weaponHolder.position, weaponHolder.transform.rotation,weaponHolder);


        currentGraphics = weaponIns.GetComponent<WeaponGraphics>();
        currentAudio = weaponIns.GetComponent<AudioSource>();

        if(currentGraphics == null)
        {
            Debug.LogError("No WeaponGraphics component on current weapon " + weaponIns.name);
        }

        if (isLocalPlayer)
        {
            Util.SetLayerRecursively(weaponIns, LayerMask.NameToLayer(weaponLayerName));
        }
    }

}
